# Jeu de la Vie

Librairies requises: pygame, astar

![Screenshot](https://framagit.org/kingu/jeu-de-la-vie/-/raw/7212122208df2111cc6f4e472f0a164bb5245a30/img/jdlv-screen.png)

# Comment jouer?

Vous déplacez votre personnage trois fois, vous placez trois pierres puis l'adversaire fait de meme.
A chaque fois qu'un personnage bouge (c-a-d vous et votre adversaire) le plateau de jeu évolue selon les
regles d'un ![automate cellulaire][https://fr.wikipedia.org/wiki/Automate_cellulaire].

Le premier a atteindre la case centrale a gagné.

Ça, ce sont les règles du premier tableau. Les autres qui suivent, vous aviserez!

# Installation

 
<div style="border-style:solid">
<h1>ATTENTION - IMPORTANT - PYTHON 3.8</h1>

<div>A ce stade ci (le 17 Mai 2020) la plus récente version de Python (3.8) N'EST PAS COMPATIBLE avec Pygame.  
Vous devez installer la 3.7. (voir capture ci-bas)</div>
<div>
<br/>

![Python download page](https://framagit.org/kingu/jeu-de-la-vie/-/raw/master/img/p38warning.png)



## :red_circle: Linux

- Clonez ou dezippez le jeu
- installez Python et les librairies requises en exécutant les commandes suivantes:
```
$  sudo apt-get install python3
$  sudo pip3 install astar
$  sudo pip3 install pygame
$  cd [répertoire ou vous avez installé le jeu]
$  chmod 755 play.sh
```
- Vous pouvez maintenant partir le jeu en executant play.sh

## :large_blue_circle: Windows

- Installez Python (téléchargez et suivez les instructions sur sur python.org).
- installez les librairies requises en tappant les truc suivants dans un terminal
```
$  pip install astar
$  pip install pygame
```
- Vous pouvez maintenant partir le jeu en executant play.bat

## :black_circle: MacOS 

- Installez Python (téléchargez et suivez les instructions sur sur python.org).
- Ouvrez un terminal avec ⌘-t et entrez-y les commandes suivantes
```
$  pip3 install astar
$  pip3 install pygame
$  cd [répertoire ou vous avez installé le jeu]
$  chmod 755 play.sh
```
- Vous pouvez maintenant partir le jeu en executant play.sh


Peu importe quel os, le commande play prend une seul argument optionel, le niveau de depart.
Pour savoir combien de niveau il y a au total, regardez dans le dossier 'scenarios' ou vous constaterez rapidement que les "scenarios" ont tous la meme syntaxe "sceXXXX.py" ou XXXX est le numéro du scénario.  Quand 9999 scénarios vont avoir été crées, le lendemain sera le rapture day.

# Credits

PC & NPC sprites from http://blogoscoped.com/archive/2006-08-08-n51.html
Other sprites from https://thewristbandit.itch.io/boxes-be-gone

This means I give credits to Philipp Lenssen, to Lorc, to Delapouite, to Viscious Speed and to TheWristbandit.

The sound effect are from https://opengameart.org/content/library-of-game-sounds and from https://www.freesoundslibrary.com/

The copyright infos about the fonts can be seen in each individual font directories.

The part that are ugly have been done by me.
