class Node:
    
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __eq__(self, other): 
        if not isinstance(other, Node):
            return NotImplemented
        return self.x == other.x and self.y == other.y

    def __str__(self):
        return "[" + str(self.x) + ": " + str(self.y) + "]"
    
    def neighbors(self, diag):
        x = self.x
        y = self.y
        result=[]
        nx = x 
        ny = y - 1
        if ny >= 0:
            result.append(Node(nx,ny))
        if diag:
            nex = x + 1
            ney = y - 1
            if nex < 30 and ney >= 0:
                result.append(Node(nex,ney))
        ex = x + 1        
        ey = y
        if ex < 30:
            result.append(Node(ex,ey))
        if diag:
            sex = x + 1        
            sey = y + 1
            if sex < 30 and sey < 30:
                result.append(Node(sex,sey))
        sx = x
        sy = y + 1
        if sy < 30:
            result.append(Node(sx,sy))
        if diag:
            sox = x - 1
            soy = y + 1
            if sox >= 0 and soy < 30:
                result.append(Node(sox,soy))
        ox = x - 1
        oy = y
        if ox >= 0:
            result.append(Node(ox,oy))
        if diag:
            nox = x - 1
            noy = y - 1
            if nox >= 0 and noy >= 0:
                result.append(Node(nox,noy))
                
        return result

    def toCoor(self):
        x = (self.x * 25) + 50
        y = (self.y * 25) + 25
        return (x, y)
