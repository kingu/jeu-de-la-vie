import node

N = node.Node

def r_pentomino(pos):
    #  **
    # **
    #  *
    x = pos.x
    y = pos.y
    return [N(x + 1, y), N(x + 2, y),
            N(x, y + 1), N(x + 1, y + 1),
            N(x + 1, y + 2)] 

def pulsar(pos):
    x = pos.x
    y = pos.y
    return [ N(x + 2, y), N(x + 3, y),  N(x + 3, y), N(x + 4, y),  N(x + 8, y),  N(x + 9, y),  N(x + 10, y),
             N(x, y + 2), N(x + 5, y + 2), N(x + 7, y + 2),  N(x + 12, y + 2),
             N(x, y + 3), N(x + 5, y + 3), N(x + 7, y + 3),  N(x + 12, y + 3),
             N(x, y + 4), N(x + 5, y + 4), N(x + 7, y + 4),  N(x + 12, y + 4),             
             N(x + 2, y + 5), N(x + 3, y + 5),  N(x + 3, y + 5), N(x + 4, y + 5),
             N(x + 8, y + 5), N(x + 9, y+ 5),  N(x + 10, y + 5),
             N(x + 2, y + 7), N(x + 3, y + 7),  N(x + 3, y + 7), N(x + 4, y + 7),
             N(x + 8, y + 7), N(x + 9, y + 7),  N(x + 10, y + 7),
             N(x, y + 8), N(x + 5, y + 8), N(x + 7, y + 8),  N(x + 12, y + 8),
             N(x, y + 9), N(x + 5, y + 9), N(x + 7, y + 9),  N(x + 12, y + 9),
             N(x, y + 10), N(x + 5, y + 10), N(x + 7, y + 10),  N(x + 12, y + 10),             
             N(x + 2, y + 12), N(x + 3, y + 12),  N(x + 3, y + 12), N(x + 4, y + 12),
             N(x + 8, y + 12), N(x + 9, y + 12),  N(x + 10, y + 12)]




    
