class Character():
    def __init__(self, pos, goal, moves, dots):
        self.pos = pos
        self.start = pos
        self.goal = goal
        self.moves = moves
        self.movesLeft = moves
        self.dots = dots
        self.dotsLeft = dots        
