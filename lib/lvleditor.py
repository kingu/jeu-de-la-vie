import node
import pygame
import pathfinder
import time
import random
import sys
import game
import copy


class Editor(game.Game):

    def __init__(self):
        super().__init__()
        self.action = "edit"
        self.status = "editing"
        self.initSelf = []
        self.evolved = False
        pygame.display.set_caption('Le jeu de la vie!')
        
    def showMousePos(self):
        print (self.getMousePos())
    
    def drawBoard(self, hooks=False):

        # define the basic gameboard
        gameboard = pygame.Surface(self.screen.get_size())
        gameboard.blit(self.templateImg, (0, 0))

        # blit the pc goal 
#        gameboard.blit(self.pcGoalImg, self.pc.goal.toCoor())

        # blit all the black cells
        for n in self.bcells:
            gameboard.blit(self.bcellImg, n.toCoor())

        # blit all the perm cells
        for n in self.pcells:
            gameboard.blit(self.pcellImg, n.toCoor())

            
        # blit the pc
#        gameboard.blit(self.pcImg, self.pc.pos.toCoor())

        # blit all the npc
#        for n in self.npc:
#            gameboard.blit(self.npcImg, n.pos.toCoor())
            
#        if self.action == "pc_move":
            # blit the movement indicators
#            for n in self.pc.pos.neighbors(False):
#                if self.validatePcMoveClick(n):
#                if not n in self.bcells and not n in self.pcells:
#                    gameboard.blit(self.pcmoveImg, n.toCoor())
                    
#        if self.action == "pc_placedot":
            # blit all the placedotable spaces 
#            for x in range(30):
#                for y in range(30):
#                    n = node.Node(x, y)
#                    if self.validatePlacedot(n):
#                        gameboard.blit(self.pcplacedotImg, n.toCoor())


        # blit the level and turn number indicators
#        text = self.font.render("Niveau: " + str(self.level), True, (193, 73, 73), (0, 0, 0))
#        gameboard.blit(text, (825, 240))
        
#        text = self.font.render("Tour: " + str(self.turnCount), True, (193, 73, 73), (0, 0, 0))
#        gameboard.blit(text, (825, 270))

        # blit the objective
#        text = self.smallfont.render("Objectif: " + self.objective, True, (193, 73, 73), (0, 0, 0))
#        gameboard.blit(text, (50, 780))
        
        # set status to "playing"
#        self.status = "playing"
        
#        self.customRendering(gameboard)
        
        # Convert and flip the gameboard
        gameboard = gameboard.convert()
        self.screen.blit(gameboard, (0,0))
        pygame.display.flip()

    def evolve(self, fakerun = False):
        old = self.bcells + self.pcells
        new = []
        if not self.evolved:
            self.initSelf = [copy.copy(self.bcells),copy.copy(self.pcells),copy.copy(self.gcells)]
            self.evolved = True
        for x in range(30):
            for y in range(30):
                cn = node.Node(x,y)
                nb = cn.neighbors(True)
                i = 0
                for n in nb:
                    if n in old:
                        i = i + 1
                if (not cn in old and i == 3) or (cn in old and (i == 2 or i == 3)):
                    new.append(cn)

        # Remove new black cells that are located on a ghost cell space, a perm cell space or the goal
        for n in new:
            if n in self.gcells or n in self.pcells: # or n == self.pc.goal:
                new.remove(n)

        # Unless it is a fakerun, update the board.        
        if not fakerun:

            self.updateBoard(new)
        return new

    def reset(self):
        self.bcells = self.initSelf[0]
        self.pcells = self.initSelf[1]
        self.gcells = self.initSelf[2]
    
    def updateBoard(self, new):
        self.bcells = new
        
    def processClick(self):
        mp = self.getMousePos()
        if mp:
            if mp in self.bcells:
                self.bcells.remove(mp)
            else:
                self.bcells.append(mp)
            self.drawBoard()
                
        # if mp:
        #     if self.action == "edit":
        #         self.executePcMoveEvent(mp)
        #     elif self.action == "pc_placedot":
        #         self.executePcPlacedotEvent(mp)

