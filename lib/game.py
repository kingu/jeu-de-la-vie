import node
import pygame
import pathfinder
import time
import random
import sys

class Game():

    def __init__(self):
        random.seed()
        self.turnCount = 0
        self.screen = pygame.display.set_mode((1000, 825))
        self.templateImg = pygame.image.load("./img/tpl.jpg")
        self.pcImg = pygame.image.load("./img/pc.png")
        self.npcImg = pygame.image.load("./img/npc.png")
        self.bcellImg = pygame.image.load("./img/bcell.png")
        self.pcellImg = pygame.image.load("./img/pcell.png")
        self.ecellImg = pygame.image.load("./img/ecell.png")
        self.pcmoveImg = pygame.image.load("./img/move.png")
        self.pcplacedotImg = pygame.image.load("./img/placedot.png")
        self.pcGoalImg = pygame.image.load("./img/goal.png")
        self.backhomeImg = pygame.image.load("./img/backhome.png")
        self.pcMoveSnd = pygame.mixer.Sound("./snd/pcmove.ogg")
        self.npcMoveSnd = pygame.mixer.Sound("./snd/npcmove.ogg")
        self.placedotSnd = pygame.mixer.Sound("./snd/placedot.ogg")
        self.pcdieSnd = pygame.mixer.Sound("./snd/pcdie.ogg")
        self.evolutionSnd = pygame.mixer.Sound("./snd/evolve.ogg")
        self.npcdieSnd = pygame.mixer.Sound("./snd/npcdie.ogg")
        self.gameoverWinSnd = pygame.mixer.Sound("./snd/gameoverwin.ogg")
        self.gameoverLoseSnd = pygame.mixer.Sound("./snd/gameoverlose.ogg")
        self.font = pygame.font.Font("./font/zorque/zorque.ttf", 24)
        self.smallfont = pygame.font.Font("./font/zorque/zorque.ttf", 12)
        self.hugefont = pygame.font.Font("./font/zorque/zorque.ttf", 50)
        self.name = False
        self.objective = False
        self.level = False
        self.bcells = [] # black cells = regular positive cells
        self.gcells = [] # ghost cells = are always negative
        self.pcells = [] # permanent cells = are always positive
        self.ecells = [] # empty cells = blocking cells that do not count as positive
        self.pc = False # must be defined in subclass
        self.npc = []
        self.action = "pc_move"
        self.status = "new_game"
        pygame.display.set_caption('Le jeu de la vie!')
        
    def getMousePos(self):
        clickPos = pygame.mouse.get_pos()
        x = clickPos[0]
        y = clickPos[1]
        if x > 50 and x < 800 and y > 25 and y < 775:
            cx = int((x - 50) / 25)
            cy = int((y - 25) / 25)
            return node.Node(cx,cy)
        elif x > 800 and y > 695  and  y < 750:
            if x < 865:
                self.status = "game_over_lose"
                return False
            elif x < 930:
                print ("Not implemented yet...")
                return False
            else:
                print ("FUCK THIS GAME!!!!!")
                exit()
        else:
            return False

    def isBlack(self, nd):
        if nd in self.bcells or nd in self.pcells or nd in self.ecells:
            return True
        else:
            return False

    def validatePcMoveClick(self, mp):
        if mp in self.bcells or mp in self.pcells  or mp in self.ecells or not mp in self.pc.pos.neighbors(False):
            return False
        else:
            return True

    def evolve(self, fakerun = False):
        old = self.bcells + self.pcells
        new = []
        for x in range(30):
            for y in range(30):
                cn = node.Node(x,y)
                nb = cn.neighbors(True)
                i = 0
                for n in nb:
                    if n in old:
                        i = i + 1
                if ((not cn in old and i == 3) or (cn in old and (i == 2 or i == 3))) and not cn in self.gcells and not cn in self.pcells and not cn == self.pc.goal:
                    #print ("adding: ", cn)
                    new.append(cn)

        if not fakerun:
            for n in new:
                self.screen.blit(self.pcplacedotImg, n.toCoor())
                pygame.display.update()
            self.evolutionSnd.play()
            time.sleep(0.3)
            self.updateBoard(new)
        return new

    def pcDeath(self):
        self.pcdieSnd.play()
        self.screen.blit(self.backhomeImg, node.Node(self.pc.pos.x - 1, self.pc.pos.y - 1).toCoor())
        pygame.display.update()
        time.sleep(0.5)
        self.pc.pos = self.pc.start

    def npcDeath(self, n):
        self.npcdieSnd.play()
        self.screen.blit(self.backhomeImg, node.Node(n.pos.x - 1, n.pos.y - 1).toCoor())
        pygame.display.update()
        time.sleep(0.5)
        n.pos = n.start

        
    def updateBoard(self, new):
        self.bcells = new
        if self.pc.pos in self.bcells:
            self.pcDeath()
        for n in self.npc:
            if n.pos in self.bcells:
                self.npcDeath(n)
                
    def executePcMoveEvent(self, mp, bypassClickValidation=False ):
        if self.validatePcMoveClick(mp) or bypassClickValidation:
            print(">>>", self.pc.movesLeft)
            self.pc.pos = mp
            self.evolve()
            self.pc.movesLeft = self.pc.movesLeft - 1
            if self.pc.movesLeft == 0:
                self.action = "pc_placedot"
                self.pc.movesLeft = self.pc.moves
            self.pcMoveSnd.play()
            if mp == self.pc.goal:
                print ("Player have won")
                self.action = "pc_win"
                self.status = "game_over_win"
            self.drawBoard() 
            
    def validatePlacedot(self, mp):
        if mp == self.pc.goal:
            return False
        if mp.x < 0 or mp.x > 29 or mp.y < 0 or mp.y > 29:
            return False
        if mp in self.bcells or mp in self.pcells or mp in self.gcells  or mp in self.ecells:
            return False
        if abs(mp.x - self.pc.pos.x) <= 3 and abs(mp.y - self.pc.pos.y) <= 3:
            return False
        for npc in self.npc:
            if  abs(npc.pos.x - mp.x) <= 3 and abs(npc.pos.y - mp.y) <= 3 :
                return False
        return True

    def validatePcPlacedotClick(self, mp):
        return self.validatePlacedot(mp)
            
    def executePcPlacedotEvent(self, mp):
        if self.validatePcPlacedotClick(mp):
            self.bcells.append(mp)
            self.pc.dotsLeft = self.pc.dotsLeft - 1
            self.placedotSnd.play()
            self.drawBoard()
            if self.pc.dotsLeft == 0:
                self.pc.dotsLeft = self.pc.dots
                self.action = "ai_play"
                self.aiPlay()
            

    def findPath(self, startNode, endNode):
        # The reason for the following is that the astar library do not provide a mechanism
        # for the case of unsolvable maze.  If there is no path, It raise a type error.
        # In our case, if the maze is unsolvable, the program must not crash with a typer error but use an
        # alternate way to find it's path.  This function do not provide he alternate way. It return False
        # if no path are found.
        ms = pathfinder.Pathfinder(self)
        try:
            return list(map(ms.nodify, ms.astar(ms.tuplelify(startNode), ms.tuplelify(endNode))))
        except TypeError:
            return False
        except:
            raise
            
    def aiPlayMove(self, npc):
        targetNode = False
        if npc.goal == "pc":
            goal = self.pc.pos
        else:
            goal = npc.goal

        path = self.findPath(npc.pos, goal)
        if path:
            targetNode = path[1]
        else:
            nb = list(filter(lambda zz: not self.isBlack(zz), npc.pos.neighbors(False)))
            if len(nb) > 0:
                targetNode = nb[0]
        
        if targetNode == goal:
            print ("Super AI have won")
            self.action = "npc_win"
            self.status = "game_over_lose"
        else:
            # Make targetNode the new npc position and proceed
            npc.pos = targetNode
            self.evolve()
            self.npcMoveSnd.play()
            self.drawBoard()
            time.sleep(0.5)
            
    def aiPlacedot(self, n):
        self.bcells.append(n)
        self.placedotSnd.play()
        self.drawBoard()
        time.sleep(0.5)
        
    def aiPlacedotsA(self, pth, dl):
        if dl > 0 and len(pth) > 0:
            n = pth.pop(0)
            if self.validatePlacedot(n):
                self.aiPlacedot(n)
                dl = dl - 1
                nb = n.neighbors(False)
                random.shuffle(nb)
                pth = nb + pth
            self.aiPlacedotsA(pth, dl)

    def aiPlacedotsB(self, pos, dotsleft):
        p = random.choices(pos.neighbors(False))[0]
        #print(p)
        if self.validatePlacedot(p):
            self.aiPlacedot(p)
            dotsleft = dotsleft - 1
        if dotsleft <= 0:
            return
        else:
            self.aiPlacedotsB(p, dotsleft)
        
    def aiPlayPlacedots(self, npc, goal=False):
        #print ("asdfdf")
        # if not goal:
        #     goal = self.pc.goal
        # # self.aiPlacedotAux(self.pc.pos, 4, npc, npc.dots, random.choice([True, False]))
        # pth = self.findPath(self.pc.pos, goal)
        # if not pth:
        #     pth = self.pc.goal.neighbors(True)
        self.aiPlacedotsB(self.pc.pos, npc.dots)
            
    def aiPlay(self):
        for npc in self.npc:
            for zz in range(0,npc.moves):
                self.aiPlayMove(npc)
            self.aiPlayPlacedots(npc)
        if self.action == "npc_win":
            return
        else:
            self.nextTurn()
            
    def nextTurn(self):
        self.action = "pc_move"
        self.turnCount = self.turnCount + 1
        self.drawBoard()
        
    def customRendering(self, g=False):
        # This method is called near the end of drawBoard()
        # It is used to add custom renderings to a scenario without having to
        # redefine the drawBoard method.
        return
    
    def drawBoard(self, hooks=False):

        if self.status == "game_over_win" or self.status == "game_over_lose": 
            return

        # define the basic gameboard
        gameboard = pygame.Surface(self.screen.get_size())
        gameboard.blit(self.templateImg, (0, 0))

        
        # blit the pc goal 
        gameboard.blit(self.pcGoalImg, self.pc.goal.toCoor())

        # blit all the black cells
        for n in self.bcells:
            gameboard.blit(self.bcellImg, n.toCoor())

        # blit all the perm cells
        for n in self.pcells:
            gameboard.blit(self.pcellImg, n.toCoor())

        # blit all the empty cells
        for n in self.ecells:
            gameboard.blit(self.ecellImg, n.toCoor())


        self.customRendering(gameboard)

        # blit the pc
        gameboard.blit(self.pcImg, self.pc.pos.toCoor())

        # blit all the npc
        for n in self.npc:
            gameboard.blit(self.npcImg, n.pos.toCoor())
            
        if self.action == "pc_move":
            # blit the movement indicators
            for n in self.pc.pos.neighbors(False):
                if self.validatePcMoveClick(n):
#                if not n in self.bcells and not n in self.pcells:
                    gameboard.blit(self.pcmoveImg, n.toCoor())
                    
        if self.action == "pc_placedot":
            # blit all the placedotable spaces 
            for x in range(30):
                for y in range(30):
                    n = node.Node(x, y)
                    if self.validatePlacedot(n):
                        gameboard.blit(self.pcplacedotImg, n.toCoor())


        # blit the level and turn number indicators
        text = self.font.render("Niveau: " + str(self.level), True, (193, 73, 73), (0, 0, 0))
        gameboard.blit(text, (825, 240))
        
        text = self.font.render("Tour: " + str(self.turnCount), True, (193, 73, 73), (0, 0, 0))
        gameboard.blit(text, (825, 270))

        # blit the objective
        text = self.smallfont.render("Objectif: " + self.objective, True, (193, 73, 73), (0, 0, 0))
        gameboard.blit(text, (50, 780))
        
        # set status to "playing"
        self.status = "playing"
                
        # Convert and flip the gameboard
        gameboard = gameboard.convert()
        self.screen.blit(gameboard, (0,0))
        pygame.display.flip()
        
    def processClick(self):
        mp = self.getMousePos()
        if mp:
            if self.action == "pc_move":
                self.executePcMoveEvent(mp)
            elif self.action == "pc_placedot":
                self.executePcPlacedotEvent(mp)

    def showEndScreen(self):
        if self.status == "game_over_win":
            text = "VICTOIRE!!!"
            loc = (300, 300)
            snd = self.gameoverWinSnd
        else:
            text = "Vous avez perdu!!!"
            loc = (200, 300)
            snd = self.gameoverLoseSnd
        rtext = self.hugefont.render(text, True, (193, 73, 73))
        self.screen.blit(rtext, loc)
        pygame.display.update()
        snd.play()
        time.sleep(5)
