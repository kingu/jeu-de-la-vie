from astar import AStar
import math
import node
class Pathfinder(AStar):
    def __init__(self, g):
        
        self.cells = g.bcells + g.pcells + g.evolve(True)
        self.width = 30
        self.height = 30

    def tuplelify (self, n):
        return (n.x, n.y)

    def nodify (self, tup):
        (x, y) = tup
        return node.Node(x, y)

    def heuristic_cost_estimate(self, n1, n2):
        """computes the 'direct' distance between two (x,y) tuples"""
        (x1, y1) = n1
        (x2, y2) = n2
        return math.hypot(x2 - x1, y2 - y1)

    def distance_between(self, n1, n2):
        """this method always returns 1, as two 'neighbors' are always adajcent"""
        return 1

    def walkable(self, nd, bnl):
        if nd in bnl:
            return False
        else:
            return True
        
    def neighbors(self, n):
        """ for a given coordinate in the maze, returns up to 4 adjacent(north,east,south,west)
            nodes that can be reached (=any adjacent coordinate that is not a wall)
        """
        bn = self.cells
        fnbl = list(filter (lambda zz: self.walkable(zz, bn), self.nodify(n).neighbors(False)))
        lfnbl = list(map (lambda zz: self.tuplelify(zz), fnbl))
        return lfnbl
