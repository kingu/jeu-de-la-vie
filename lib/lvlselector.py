import os
import node
import pygame
from pygame.locals import *

class N(node.Node):
    def drawImage(self, scrn, image):
        scrn.blit(image, self.toCoor())
        

class Lvl():
    def __init__(self, idx):
        self.idx = idx
        # check for file sceXXXX.py
        exists = True
        if exists:
            self.trn = 23
            self.stars = 3
        else:
            self.trn = False
            self.stars = False
        
            
class LvlSelector():
    def __init__(self):
        # check the ./scenario directory to fetch the maxLvl
        self.maxLvl = 3
        # check userfiles to fetch the history data
        # (a list of Lvl obj)
        self.history = []
        for s in range(self.maxLvl + 1):
            self.history.append(Lvl(s))
        
        self.screen = pygame.display.set_mode((1000, 825))
        self.templateImg = pygame.image.load("./img/tpl.jpg")
        self.bcellImg = pygame.image.load("./img/bcell.png")
        self.pcellImg = pygame.image.load("./img/pcell.png")

    def getMousePos(self):
        clickPos = pygame.mouse.get_pos()
        x = clickPos[0]
        y = clickPos[1]
        if x > 50 and x < 800 and y > 25 and y < 775:
            cx = int((x - 50) / 25)
            cy = int((y - 25) / 25)
            return node.Node(cx,cy)
        else:
            return False

    def drawPage(self, page=0):
        # if page > 3 / 20:
        #     page = self.maxLvl
        # page = max(page, 0)
        print (self)
        lvlSelectScrn = pygame.Surface(self.screen.get_size())
        lvlSelectScrn.blit(self.templateImg, (0, 0))         
        zz = [5, 11, 17, 23, 29]
        for y in range(30):
            if y in zz:
                for x in range(29):
                    N(x, y).drawImage(lvlSelectScrn, self.bcellImg)
            else:
                for x in zz:
                    N(x, y).drawImage(lvlSelectScrn, self.bcellImg)
            N(29, y).drawImage(lvlSelectScrn, self.pcellImg)
        lvlSelectScrn = lvlSelectScrn.convert()
        self.screen.blit(lvlSelectScrn, (0,0))
        pygame.display.flip()
        
    
