#!/usr/bin/python3.7

import pygame
from pygame.locals import *
import sys
import os
import lvlselector
from pathlib import Path
import lvleditor

pygame.init()

    
def main():

    GAME = lvleditor.Editor()
    
    GAME.drawBoard()
    omp = False    
    # Event loop
    while 1:
        mp = GAME.getMousePos()
        for event in pygame.event.get():
            if mp and not mp == omp:
                omp = mp
                GAME.showMousePos()
            if event.type == QUIT:
                return

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    print("Space")
                    GAME.evolve()
                    GAME.drawBoard()
                elif event.key == pygame.K_ESCAPE:
                    print ("escape")
                    GAME.reset()
                    GAME.drawBoard() 

            elif event.type == pygame.MOUSEBUTTONDOWN:
                GAME.processClick()
                
if __name__ == '__main__': main()
