#!/usr/bin/python3.7

import os
import pygame
from pygame.locals import *
import sys
import lvlselector
from pathlib import Path

print ("+-----+\n|  *  | Le Jeu\n|    *| De la (sur)Vie \n|* * *| Version ALPHA\n+-----+\nPar: JP Theberge\nCe jeu est distribué sous la licence libre GPL3\nvoir le fichier LICENCE pour les détails.\n\nhttps://framagit.org/kingu/jeu-de-la-vie")

pygame.init()
HOME = str(Path.home()) + "/.jdlv/"

def loadScenario(num):
    try:
        modname ="sce" + str(num).rjust(4, '0')
        module = __import__(modname)
        sce = getattr(module, modname)
        return sce()
    except ModuleNotFoundError:
        print ("Vous avez terminé le jeu!! Félicitation!")
        exit()
    except:
        raise

def saveScore(snum, tnum):
    sf = HOME + str(snum)
    if os.path.isfile(sf):
        f = open(sf, "r")
        v = f.read()
        f.close()
        bscore = max(int(v), tnum)
    else:
        bscore = tnum
    f = open(sf, "w")
    f.write(str(bscore))
    f.close()

def getNextLvl(sl):
    sf = HOME + str(sl)
    if os.path.isfile(sf):
        return getNextLvl(sl + 1)
    else:
#        print(sl)
        return sl

def main():

    # Create a .jdlv directory on the user home for savegame
    try:
        os.mkdir(HOME)
    except FileExistsError:
        pass
    except:
        raise
        
    if len(sys.argv) == 2:
        scenario = int(sys.argv[1])
    else:
        scenario = getNextLvl(0)

    GAME = loadScenario(scenario)
    
    GAME.drawBoard()
    
    # Event loop
    while 1:
        if GAME.status == "game_over_lose":
            print ("Game Over. Restarting...")
            GAME.showEndScreen()
            GAME = loadScenario(scenario)
            GAME.drawBoard()
        if GAME.status == "game_over_win":
            print ("Loading next scenario")
            GAME.showEndScreen()
            saveScore(scenario, GAME.turnCount)
            scenario = scenario + 1
            GAME = loadScenario(scenario)
            GAME.drawBoard()
        for event in pygame.event.get():
            if event.type == QUIT:
                return

            elif event.type == pygame.MOUSEBUTTONDOWN:
                GAME.processClick()

if __name__ == '__main__': main()
