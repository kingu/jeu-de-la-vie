import game
import node
import character
import patterns

N = node.Node

class sce0007(game.Game):
    def __init__(self):
        game.Game.__init__(self)
        self.name = "Pentomino"
        self.objective = "Rejoindre la case centrale avant l'adversaire"
        self.level = 7
        #        self.bcells = self.r_pentomino(N(15, 15)) + self.r_pentomino(N(5, 5)) 
        self.bcells = patterns.pulsar(N(5,5))
        self.gcells = [node.Node(29,29), node.Node(29,28), node.Node(28,29), node.Node(28,28)]
        self.pcells = [ N(22, 21), N(22, 20), N(22,19), N(22, 18), N(21, 22), N(20, 22), N(19, 22), N(18, 22)]
        self.ecells = []
        self.pc = character.Character(N(2,2), N(20,20), 1, 1)
        self.npc = [character.Character(node.Node(27,27), node.Node(20,20), 1, 1)] 
        for n in range(30):
            self.ecells = self.ecells + [ N(n,0), N(n, 29) ]
            if not n in [0,29]:
                self.ecells = self.ecells + [ N(0,n), N(29, n) ]

    def nextTurn(self):
        self.action = "pc_move"
        self.turnCount = self.turnCount + 1
        self.pc.moves =  self.pc.moves + 1
        self.pc.dots  = self.pc.dots + 1
        self.pc.movesLeft = self.pc.moves
        self.pc.dotsLeft = self.pc.dots

        for npc in self.npc:
            npc.moves =  npc.moves + 1
            npc.dots  = npc.dots + 1
        self.drawBoard()

    def pcDeath(self):
        self.pc.moves = max(1, self.pc.moves - 1)
        self.pc.dots = max(1, self.pc.dots - 1)
        for npc in self.npc:
            npc.moves = max(1, npc.moves + 1)
            npc.dots  = max(1, npc.dots + 1)
        super().pcDeath()

    def npcDeath(self, n):
        self.pc.moves = max(1, self.pc.moves + 1)
        self.pc.dots = max(1, self.pc.dots + 1)
        for npc in self.npc:
            npc.moves = max(1, npc.moves - 1)
            npc.dots  = max(1, npc.dots - 1)
        super().npcDeath(n)
 
    def customRendering(self, gameboard):
        text = self.font.render("PC: " + str(self.pc.moves), True, (193, 73, 73), (0, 0, 0))
        gameboard.blit(text, (825, 300))
        text = self.font.render("NPC: " + str(self.npc[0].moves), True, (193, 73, 73), (0, 0, 0))
        gameboard.blit(text, (825, 350))

    
