import pygame
import game
import node
import character
import random
import time

# Regles
# au depart, une bombe est place en 5-5
# Quand le pc ou un npc meurt, une bombe est place sur le jeu sur une case aleatoire vide
# Si une cellule apparais sur une bombe, la bombe explose (incluant le pc qui en transporte)
# idem si on place volontairement une cellule sur une bombe.


class Bomber(character.Character):
    def __init__(self, pos, goal, moves, dots):
        super().__init__(pos, goal, moves, dots)
        self.bombs = 2
    
class sce0004(game.Game):
    def __init__(self):
        game.Game.__init__(self)
        self.name = "Tutoriel"
        self.objective = "Rejoindre la case centrale avant l'un des adversaires"
        self.level = 4

        self.bcells = [node.Node(15,12), node.Node(16,13), node.Node(17,14), node.Node(18,15), node.Node(17,16),
                       node.Node(16,17), node.Node(15,18), node.Node(14,17), node.Node(13,16), node.Node(12,15),
                       node.Node(13,14), node.Node(14,13), node.Node(15,14), node.Node(16,15), node.Node(15,16),
                       node.Node(14,15)]
        self.gcells = [node.Node(0,0), node.Node(0,1), node.Node(1,0), node.Node(1,1),
                       node.Node(29,29), node.Node(29,28), node.Node(28,29), node.Node(28,28)]
        self.pcells = []
        self.pc = Bomber(node.Node(0,0), node.Node(15,15), 3, 3)
        self.npc = [character.Character(node.Node(29,29), node.Node(15,15), 3, 3),
                    character.Character(node.Node(29,0), node.Node(15,15), 3, 3),
                    character.Character(node.Node(0,29), node.Node(15,15), 3, 3)] 
        self.bombImg = pygame.image.load("./img/bomb.png")
        self.bombIndicatorImg = pygame.image.load("./img/bombindicator.png")
        self.explosionImg = pygame.image.load("./img/explosion.png")
        self.bombExplodeSnd = pygame.mixer.Sound("./snd/bomb.ogg")
        self.bombsLoc = [node.Node(3,3)]
        
    def explode(self, bombPos):
        if bombPos in self.bcells:
            self.bcells.remove(bombPos)
        for n in bombPos.neighbors(True):
            if n in self.bcells:
                self.bcells.remove(n)
            for npc in self.npc:
                if npc.pos == n:
                    self.npcdieSnd.play()
                    npc.pos == npc.start
        self.bombExplodeSnd.play()
        self.screen.blit(self.explosionImg, node.Node(bombPos.x - 1, bombPos.y - 1).toCoor())
        pygame.display.update()
        if bombPos in self.bombsLoc:
            self.bombsLoc.remove(bombPos)
        time.sleep(0.2)

    def updateBoard(self, new):
        self.bcells = new
        newbombs=[]
        if self.pc.pos in self.bcells:
            if self.pc.bombs > 0:
                self.explode(self.pc.pos)
                self.pc.bombs = self.pc.bombs - 1
            else:
                self.pcdieSnd.play()
                newbombs.append(self.pc.pos)                
                self.pc.pos = self.pc.start
        for n in self.npc:
            if n.pos in self.bcells:
                self.npcdieSnd.play()
                newbombs.append(n.pos)                
                n.pos = n.start
                self.placeRandomBomb()
        for b in self.bombsLoc:
            if b in self.bcells:
                self.explode(b)
            elif b == self.pc.pos:
                self.pc.bombs = self.pc.bombs + 1
                self.bombsLoc.remove(b)
        self.bombsLoc = self.bombsLoc + newbombs
                
    def placeRandomBomb(self):
        x=random.choices(range(29))
        y=random.choices(range(29))
        n=node.Node(x[0],y[0])
        if n in self.bcells or n in self.bombsLoc:
            self.placeRandomBomb()
        else:
            self.bombsLoc.append(n)
        
        
    def customRendering(self, gameboard):
            text = self.font.render("Bombes: " + str(self.pc.bombs), True, (193, 73, 73), (0, 0, 0))
            gameboard.blit(text, (825, 300))
            if self.pc.bombs > 0:
                gameboard.blit(self.bombIndicatorImg, self.pc.pos.toCoor())
            for b in self.bombsLoc:
                print ("BL ", b)
                gameboard.blit(self.bombImg, b.toCoor())

        
