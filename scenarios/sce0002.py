import game
import node
import character
import pygame

N = node.Node

class ChainsawWarrior(character.Character):
    def __init__(self, pos, goal, moves, dots):
        super().__init__(pos, goal, moves, dots)
        self.haveChainsaw = False
        self.gaztanks = 0
        
class sce0002(game.Game):
    def __init__(self):
        game.Game.__init__(self)
        self.name = "Tutoriel"
        self.objective = "Rejoindre la case centrale avant l'un des adversaires"
        self.level = 2
        self.bcells = [node.Node(15,13),  node.Node(15,14),  node.Node(13,15),
                       node.Node(14,15), node.Node(16,15), node.Node(17,15),  node.Node(15,16),
                       node.Node(15,17)]
        self.gcells = [node.Node(0,0), node.Node(0,1), node.Node(1,0), node.Node(1,1),
                       node.Node(28,0), node.Node(29,0), node.Node(28,1), node.Node(29,1),
                       node.Node(0,28), node.Node(0,29), node.Node(1,28), node.Node(1,29),
                       node.Node(5,5), N(15, 14), N(14, 15), N(16, 15), N(15, 16)]
        self.pcells = [node.Node(14,14), node.Node(16,14), node.Node(14,16), node.Node(16,16)]
        self.pc = ChainsawWarrior(node.Node(0,0), node.Node(15,15), 3, 3)
        self.npc = [character.Character(node.Node(29,0), node.Node(15,15), 3, 3),
                    character.Character(node.Node(0,29), node.Node(15,15), 3, 3)] 
        self.chainsawImg = pygame.image.load("./img/chainsaw.png")
        self.chainsawIndicatorImg = pygame.image.load("./img/chainsawindicator.png")
        self.chainsawSnd = pygame.mixer.Sound("./snd/chainsaw.ogg")
        self.takeSnd = pygame.mixer.Sound("./snd/take.ogg")
        self.chainsawAvailable = True

    def updateBoard(self, new):
        self.bcells = new
        if self.pc.pos in self.bcells:
            if self.pc.haveChainsaw:
                self.bcells.remove(self.pc.pos)
                self.pc.gaztanks = self.pc.gaztanks - 1
                self.chainsawSnd.play()
                if self.pc.gaztanks <= 0:
                    self.pc.haveChainsaw = False
            else:
                self.pcdieSnd.play()
                self.pc.pos = self.pc.start
                self.pc.haveChainsaw = False
                self.pc.gaztanks = 0
                self.chainsawAvailable = False
        for n in self.npc:
            if n.pos in self.bcells:
                self.npcdieSnd.play()
                n.pos = n.start
                self.chainsawAvailable = True
                
    def customRendering(self, gameboard):
        if self.pc.haveChainsaw:
            gameboard.blit(self.chainsawIndicatorImg, self.pc.pos.toCoor())
            text = self.font.render("Gaz: " + str(self.pc.gaztanks), True, (193, 73, 73), (0, 0, 0))
            gameboard.blit(text, (825, 300))
        elif self.chainsawAvailable:
            gameboard.blit(self.chainsawImg, node.Node(5,5).toCoor())
                    
    def executePcMoveEvent(self, mp):
        if self.validatePcMoveClick(mp):
            self.pc.pos = mp
            if mp in self.bcells:
                print ("Destroying cell ", mp)
                self.chainsawSnd.play()
                self.bcells.remove(mp)
                self.pc.gaztanks = self.pc.gaztanks - 1
                print ("Gaztanks ", self.pc.gaztanks)
            if mp in self.pcells and self.pc.gaztanks >= 2:
                self.pcells.remove(mp)
                self.pc.gaztanks = self.pc.gaztanks - 2 
            if mp == node.Node(5,5):
                self.takeSnd.play()
                self.pc.haveChainsaw = True
                self.pc.gaztanks = 5
                
            if mp == self.pc.goal:
                print ("Player have won")
                self.action = "pc_win"
                self.status = "game_over_win"
            self.evolve()
            self.pc.movesLeft = self.pc.movesLeft - 1
            if self.pc.movesLeft == 0:
                self.pc.movesLeft = self.pc.moves
                self.action = "pc_placedot"
            self.pcMoveSnd.play()
            if self.pc.haveChainsaw and self.pc.gaztanks <= 0:
                self.pc.haveChainsaw = False
                self.pc.gaztanks = 0
            self.drawBoard() 

    def validatePcMoveClick(self, mp):
        if not self.pc.haveChainsaw and mp in self.bcells or mp in self.pcells or not mp in self.pc.pos.neighbors(False):
            return False
        else:
            return True

            
