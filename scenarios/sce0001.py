import game
import node
import character
import pygame

class Freezer(character.Character):
    def __init__(self, pos, goal, moves, dots):
        super().__init__(pos, goal, moves, dots)
        self.freezeStatus = 1

class sce0001(game.Game):
    def __init__(self):
        super().__init__()
        self.name = "Tutoriel"
        self.objective = "Rejoindre la case centrale avant l'un des adversaires"
        self.level = 1
        self.bcells = [node.Node(15,13), node.Node(15,14),  node.Node(13,15),
                       node.Node(14,15), node.Node(16,15), node.Node(17,15),  node.Node(15,16),
                       node.Node(15,17), node.Node(14,14), node.Node(16,14), node.Node(14,16),
                       node.Node(16,16), node.Node(26,27), node.Node(27,27), node.Node(28,27),
                       node.Node(26,28), node.Node(27,29)]
        self.gcells = [node.Node(0,0), node.Node(0,1), node.Node(1,0), node.Node(1,1),
                       node.Node(28,0), node.Node(29,0), node.Node(28,1), node.Node(29,1),
                       node.Node(0,28), node.Node(0,29), node.Node(1,28), node.Node(1,29)]
        self.pcells = []
        self.pc = character.Character(node.Node(0,0), node.Node(15,15), 3, 3)
        self.npc = [Freezer(node.Node(29,0), node.Node(15,15), 3, 3),
                    Freezer(node.Node(0,29), node.Node(15,15), 3, 3)]
        self.npcImg = pygame.image.load("./img/npc01.png")
        self.frozenImg = pygame.image.load("./img/frozenindicator.png")

        
    def updateBoard(self, new):
        self.bcells = new
        if self.pc.pos in self.bcells:
            self.pcdieSnd.play()
            self.pc.pos = self.pc.start
        for n in self.npc:
            if n.pos in self.bcells:
                self.npcdieSnd.play()
                n.pos = n.start
                n.freezeStatus = 3

    def aiPlay(self):
        for npc in self.npc:
            if npc.freezeStatus == 0:
                for zz in range(0,npc.moves):
                    self.aiPlayMove(npc)
                self.aiPlayPlacedots(npc)
            else:
                npc.freezeStatus = npc.freezeStatus - 1 
        if self.action == "npc_win":
            return
        else:
            self.action = "pc_move"
            self.turnCount = self.turnCount + 1
            self.drawBoard()

    def customRendering(self, gameboard):
        for npc in self.npc:
            if npc.freezeStatus > 0:
                gameboard.blit(self.frozenImg, npc.pos.toCoor())
        
