import game
import node
import character

class sce0000(game.Game):
    def __init__(self):
        game.Game.__init__(self)
        self.name = "Tutoriel"
        self.objective = "Rejoindre la case centrale avant l'adversaire"
        self.level = 0
        self.bcells = [node.Node(15,12), node.Node(16,13), node.Node(17,14), node.Node(18,15), node.Node(17,16),
                       node.Node(16,17), node.Node(15,18), node.Node(14,17), node.Node(13,16), node.Node(12,15),
                       node.Node(13,14), node.Node(14,13), node.Node(15,14), node.Node(16,15), node.Node(15,16),
                       node.Node(14,15)]
        self.gcells = [node.Node(0,0), node.Node(0,1), node.Node(1,0), node.Node(1,1),
                       node.Node(29,29), node.Node(29,28), node.Node(28,29), node.Node(28,28)]
        self.pcells = []
        self.pc = character.Character(node.Node(0,0), node.Node(15,15), 3, 3)
        self.npc = [character.Character(node.Node(29,29), node.Node(15,15), 3, 3)] 
