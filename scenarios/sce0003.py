import game
import node
import character
import random
import time

class sce0003(game.Game):
    def __init__(self):
        game.Game.__init__(self)
        self.name = "Tutoriel"
        self.objective = "Réussir a atteindre la case centrale."
        self.level = 3
        self.bcells = [ ]
        self.gcells = []
        self.pcells = [node.Node(15,12), node.Node(16,13), node.Node(17,14), node.Node(14,13),
                       node.Node(13,14), node.Node(15,14)]
        self.pc = character.Character(node.Node(0,0), node.Node(15,15), 3, 3)
        self.npc = []
        self.goalAccess = 4

    def customRendering(self, gameboard):
        self.goalAccess = 4 - len(list(filter(lambda zz: zz in self.pcells, self.pc.goal.neighbors(False))))
        text = self.font.render("Accès: " + str(self.goalAccess), True, (193, 73, 73), (0, 0, 0))
        gameboard.blit(text, (825, 300))
        
    def aiPlay(self):
        for zz in range(3):
            if len(self.bcells) > 0:
                n = random.choice(self.bcells)
                self.pcells.append(n)
                self.bcells.remove(n)
                self.drawBoard()
                time.sleep(0.3)
        if self.goalAccess <= 0:
            self.action = "npc_win"
            self.status = "game_over_lose"
        else:
            self.action = "pc_move"
        self.turnCount = self.turnCount + 1
        self.drawBoard()
