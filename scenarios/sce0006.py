import game
import node
import character
import pygame

N = node.Node

class sce0006(game.Game):
    def __init__(self):
        game.Game.__init__(self)
        self.name = "La grace divine!"
        self.objective = "Rejoindre la case centrale avant l'adversaire"
        self.level = 6
        self.bcells = []
        self.divineGraces = 0
        self.progress = 0
        self.gcells = [N(0,0), N(0,1), N(1,0), N(1,1), N(5,5), N(29,29), N(29,28), N(28,29), N(28,28), N(14,13),
                       N(17,13), N(16, 13), N(18, 13), N(14,18), N(16,18), N(15,16), N(29,0), N(0,29), N(29,29),
                       N(6,5), N(5,6), N(6,6), N(12,11), N(13,11), N(14,11), N(12,12), N(13,12),
                       N(14,12), N(12,13), N(13,13), N(14,13), N(16, 14), N(16, 15), N(16, 16), N(16,18),
                       N(12,18), N(13,18), N(14,18), N(12,19), N(13,19), N(14,19), N(12,20), N(13,20), N(14,20)
        ]
        
        self.pcells = [N(15, 0), N(15, 1), N(15, 2), N(15, 3), N(15, 4), N(15, 5), N(15, 6), N(15, 7),
                       N(15, 8),N(15, 9),N(15, 10), N(15, 11), N(15, 12), N(15, 13), N(15, 14), N(15, 17), N(15, 18),
                       N(15, 19), N(15, 20), N(15, 21), N(15, 22), N(15, 23), N(15, 24),
                       N(15, 25),N(15, 26),N(15, 27), N(15, 28), N(15, 29), N(14, 14), 
                       N(14, 15),  N(14, 16), N(16, 16), N(14, 17), N(16, 17),
                       N(0, 14), N(1, 14), N(2, 14), N(3, 14), N(4, 14), N(5, 14), N(8, 14), N(9, 14),
                       N(10, 14), N(11, 14), N(12, 14), N(13, 14), N(0, 17), N(1, 17), N(2, 17), N(3, 17),
                       N(4, 17), N(5, 17), N(8, 17), N(9, 17), N(10, 17), N(11, 17), N(12, 17), N(13, 17),
                       N(29, 27), N(27,27), N(25, 27), N(23, 27), N(21, 27), N(19,27), N(17, 27),
                       N(28, 25), N(26,25), N(24, 25), N(22, 25), N(20, 25), N(18,25), N(16, 25),                          
        ]
        self.stairways = [N(13,12), N(17,13), N(13,19), N(16,18), N(15,16), N(29,0), N(0,29), N(29,29)]
        self.pc = character.Character(node.Node(0,0), node.Node(15,15), 3, 3)
        #self.pc = character.Character(node.Node(29,29), node.Node(15,15), 3, 3)
        self.npc = [character.Character(node.Node(29,16), node.Node(15,15), 3, 3)] 
        self.portalImg = pygame.image.load("./img/portal.png")
        self.angelImg = pygame.image.load("./img/angel.png")
        self.angelPurgeSnd = pygame.mixer.Sound("./snd/angelpurge.ogg")

    def pcDeath(self):
        if self.divineGraces > 0:
            self.mayGodBlessOurSoul()
            self.divineGraces = max(0, self.divineGraces - 1)
            if self.progress == 1:
                self.progress = 2
                self.pc.pos = N(0,29)
                self.stairways.remove(N(0,29))
        else:
            super().pcDeath()

    def mayGodBlessOurSoul(self):
            self.bcells = []
            for npc in self.npc:
                npc.pos = npc.start
            self.angelPurgeSnd.play()
            
    def executePcMoveEvent(self, mp):
        if self.validatePcMoveClick(mp):
            if mp in [N(5,5), N(6,5), N(5,6), N(6,6)]:  # first angel
                self.mayGodBlessOurSoul()
                self.divineGraces = min(3, self.divineGraces + 1)
                super().executePcMoveEvent(mp, True)
            elif mp in [N(5,24), N(6,24), N(5,25), N(6,25)] and self.progress > 0:  # second angel
                self.mayGodBlessOurSoul()
                self.divineGraces = min(3, self.divineGraces + 1)
                super().executePcMoveEvent(mp, True)
            elif mp in [N(22, 5), N(22, 6), N(23, 5),N(23,6)] and self.progress > 0:  # third angel
                self.mayGodBlessOurSoul()
                self.divineGraces = min(3, self.divineGraces + 1)
                super().executePcMoveEvent(mp, True)
                
            elif mp == node.Node(13,12): # first stairway
                self.progress = 1
                self.stairways.remove(N(13,12))
                self.stairways.remove(N(29,29))
                self.pc.start = N(29,29)
                super().executePcMoveEvent(node.Node(29,29), True)
            elif mp in [N(6, 15), N(7,15)]: # crossing the gate
                self.progress = 2
                self.pcells = self.pcells + [N(6, 14), N(7, 14)]
                self.gcells = self.gcells + [N(6, 15), N(7, 15),N(6, 16), N(7, 16), N(6, 17), N(7, 17)]
                super().executePcMoveEvent(mp, True)
            elif mp == N(13,19):
                self.progress = 3
                self.stairways.remove(N(29,29))
                self.stairways.remove(N(29,0))
                self.pc.start = N(29, 0)
                self.gcells = self.gcells + [N(22, 5), N(22, 6), N(23, 5),N(23,6)] 
                super().executePcMoveEvent(node.Node(29,0), True)
            elif mp in [N(17,13),  N(16,18)]:  
                super().executePcMoveEvent(node.Node(15,16), True)
            else:
                super().executePcMoveEvent(mp, True)
        
    def customRendering(self, gameboard):
        angelPos = [N(5,5), N(5, 24), N(5, 24), N(22, 5)][self.progress]
        gameboard.blit(self.angelImg, angelPos.toCoor())
        text = self.font.render("Grâces : " + str(self.divineGraces), True, (193, 73, 73), (0, 0, 0))
        gameboard.blit(text, (825, 300))
        for p in self.stairways:
            gameboard.blit(self.portalImg, p.toCoor())
