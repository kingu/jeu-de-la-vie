import game
import node
import character
import pygame

class sce0005(game.Game):
    def __init__(self):
        game.Game.__init__(self)
        self.name = "Tutoriel"
        self.objective = "Rejoindre la case centrale avant l'un des adversaires"
        self.level = 5
        self.bcells = []
        self.gcells = [node.Node(0,0), node.Node(0,1), node.Node(1,0), node.Node(1,1), node.Node(5,5), 
                       node.Node(29,29), node.Node(29,28), node.Node(28,29), node.Node(28,28), node.Node(18,18)]
        self.pcells = [node.Node(15,12), node.Node(16,13), node.Node(17,14), node.Node(18,15), node.Node(17,16),
                       node.Node(16,17), node.Node(15,18), node.Node(14,17), node.Node(13,16), node.Node(12,15),
                       node.Node(13,14), node.Node(14,13), node.Node(15,14), node.Node(16,15), node.Node(15,16),
                       node.Node(14,15), node.Node(15,29), node.Node(15,28), node.Node(15,27), node.Node(29,15), node.Node(28,15), node.Node(27,15)]
        self.pc = character.Character(node.Node(0,0), node.Node(27,27), 3, 3)
        self.npc = [character.Character(node.Node(29,0), node.Node(27,27), 3, 3),
                    character.Character(node.Node(0,29), node.Node(27,27), 3, 3)
                    ]
        self.portalImg = pygame.image.load("./img/portal.png")
        
    def executePcMoveEvent(self, mp):
        if self.validatePcMoveClick(mp):
            if mp == node.Node(5,5):
                super().executePcMoveEvent(node.Node(18,18), True)
            elif mp == node.Node(18,18):
                super().executePcMoveEvent(node.Node(5,5), True)
            else:
                super().executePcMoveEvent(mp, True)
                
    def customRendering(self, gameboard):
        for p in [node.Node(5,5), node.Node(18,18)]:
            gameboard.blit(self.portalImg, p.toCoor())

        
