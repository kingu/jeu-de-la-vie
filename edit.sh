#!/bin/bash

PYTHON_BIN=python3.7
BASEPATH=$(dirname $(realpath -s $0))
export PYTHONPATH="${PYTHONPATH}:${BASEPATH}/lib/:${BASEPATH}/scenarios/"

${PYTHON_BIN} ${BASEPATH}/bin/editor.py ${1} 
