#!/bin/bash

PYTHON_BIN=$(find /usr/bin/python[2-9]\.[1-9] | sort -r | head -n 1)

BASEPATH=$(dirname $(realpath -s $0))
export PYTHONPATH="${PYTHONPATH}:${BASEPATH}/lib/:${BASEPATH}/scenarios/"
export PYGAME_HIDE_SUPPORT_PROMPT="hide"
cd ${BASEPATH}
${PYTHON_BIN} ./bin/jdlv.py ${1} 
